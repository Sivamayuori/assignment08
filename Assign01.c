//This is a program to store the details of students
#include<stdio.h>

struct Student{
	char name[100];
	char subject[50];
	float marks;
};

//Obtaining the input from the user
int main(){
	int x;

	//Displaying the instruction
	printf("Please enter the number of students: ");
	//Reading the entered number of students
	scanf("%d", &x);

	printf("\n");

	//Checking if the minimum requirement is met
	if (x < 5){
		printf("There should be a minimum of 5 students \n");
		return 0;
	}

	struct Student s[x];

	//Obtaining the details of the students
	for(int i = 0; i < x; i++){
		
		printf("Please enter the name of the student: ");
		scanf("%s", &s[i].name);

		printf("Please enter the subject: ");
		scanf("%s", &s[i].subject);

		printf("Please enter the marks of the student: ");
		scanf("%f", &s[i].marks);

		printf("\n");
	}
	printf("\n\n");

	//Displaying the details of the students
	for(int i = 0; i < x; i++){
		printf("Details of Student Number: %d \n", i+1);
		printf("Name of the Student: %s \n", s[i].name);
		printf("Student's subject: %s \n", s[i].subject);
		printf("Students's marks: %.2f \n", s[i].marks);

		printf("\n");
	}
	return 0;
}
